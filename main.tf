# --- root/main.tf --- 

module "vpc" {
  source           = "./vpc"
  vpc_cidr         = "192.168.0.0/16"
  public_subnet_count  = 1
  public_cidrs     = [/*"192.168.1.0/24",*/ "192.168.2.0/24"]
  private_subnet_count  = 1
  private_cidrs     = [/*"198.168.224.0/24",*/ "192.168.255.0/24"]
  max_subnet       = 2
  access_ip        = "0.0.0.0/0"
}

module "ec2" {
  source                 = "./ec2"
  public_subnets         = module.vpc.public_subnets
  instance_type          = "t2.micro"
  key_name               = "genserver"
  user_data              = filebase64("./userdata.tpl")
  vpc_id                 = module.vpc.vpc_id
}
